# Haste

Sharing code is a good thing, and it should be _really_ easy to do it.
A lot of times, I want to show you something I'm seeing - and that's where we
use pastebins.

Haste is the prettiest, easiest to use pastebin ever made.

## Changes In This Fork

* MySQL Storage Option
* Client-side AES256 encryption
* Theme loader and switcher
* Front-end fixes/cleanup
* Gfycat key generator ([via gfycat-style-urls](https://www.npmjs.com/package/gfycat-style-urls))

## Basic Usage

Type what you want me to see, click "Save", and then copy the URL.  Send that
URL to someone and they'll see what you see.

To make a new entry, click "New" (or type 'control + n')

## Duration

Pastes will stay for 30 days from their last view.  They may be removed earlier
and without notice.

## Privacy

The server has zero knowledge of your pastes.  Pastes are encrypted and
decrypted in the browser using AES-256.

Although pastes are encrypted server-side, you are responsible for what you share.
If you publicly share something you shouldn't have, that on you ¯\_(ツ)_/¯.

## Open Source

Haste can easily be installed behind your network, and it's all open source!

* [haste-server](https://gitlab.com/TwelveCharzz/haste-server)

## Author

Code by John Crepezzi <john.crepezzi@gmail.com>
Key Design by Brian Dawson <bridawson@gmail.com>
Modifications by TwelveCharzz <twelvecharzz@pm.me>
