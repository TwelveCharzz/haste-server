const gfycatStyleUrls = require('gfycat-style-urls');
module.exports = class RandomKeyGenerator {
  constructor(options = {}) {
    this.adjectives = options.adjectives || 2;
  }
  createKey(keyLength) {
    var text = '';
    return gfycatStyleUrls.generateCombination(this.adjectives, "", true);
  }
};