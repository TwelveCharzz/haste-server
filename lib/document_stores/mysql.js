var mysql = require('mysql');
var winston = require('winston');

/* 
  CREATE TABLE `entries` (
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `key` VARCHAR(255) NOT NULL,
    `value` TEXT NOT NULL,
    `expiration` INT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `key` (`key`)
  );
*/

var MysqlDocumentStore = function(options, client) {
  this.expire = options.expire;
  if (client) {
    winston.info('using predefined mysql client');
    MysqlDocumentStore.client = client;
  } else if (!MysqlDocumentStore.client) {
    winston.info('configuring mysql');
    MysqlDocumentStore.connect(options);
  }
};

// Create a connection according to config
MysqlDocumentStore.connect = function(options) {
  var host = options.host || '127.0.0.1';
  var port = options.port || 3306;
  var user = options.user || 'root';
  var pass = options.pass || '';
  var db = options.db || 'hastebin';

  MysqlDocumentStore.client = mysql.createConnection({
    host: host,
    port: port,
    user: user,
    password: pass,
    database: db
  });

  MysqlDocumentStore.client.connect(function(err) {
    if (err) {
      winston.error(
        'error connecting to mysql database ' + db,
        { error: err }
      );
      process.exit(1);
    }
    winston.info('connected to mysql on ' + host + ':' + port + '/' + db);
  });

  // Stop MySQL from losing connection
  setInterval(function() {
    // Prob shouldn't do this but ¯\_(ツ)_/¯
    MysqlDocumentStore.client.query('SELECT 1');
  }, 60000);
};

// Save file in a key
MysqlDocumentStore.prototype.set = function(key, data, callback, skipExpire) {
  var _this = this;
  var now = Math.floor(new Date().getTime() / 1000);
  MysqlDocumentStore.client.query('INSERT INTO `entries` (`key`, `value`, `expiration`) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE `value` = ?', [key, data, _this.expire && !skipExpire ? _this.expire + now : null, data], function (err, results) {
    if (err) {
      winston.error('error persisting value to mysql', { error: err });
      return callback(false);
    }
    callback(true);
  });
};

// Get a file from a key
MysqlDocumentStore.prototype.get = function(key, callback, skipExpire) {
  var _this = this;
  var now = Math.floor(new Date().getTime() / 1000);
  MysqlDocumentStore.client.query('SELECT `id`, `value`, `expiration` FROM `entries` WHERE `key` = ? AND (`expiration` IS NULL OR `expiration` > ?)', [key, now], function (err, result, fields) {
    if (err) {
      winston.error('error retrieving value to mysql', { error: err });
      return callback(false);
    }
    callback(result.length ? result[0].value : false);
    if (result.length && _this.expireJS && !skipExpire) {
      MysqlDocumentStore.client.query('UPDATE `entries` SET `expiration` = ? WHERE `id` = ?', [_this.expireJS + now, result.id], function (err) {
        if (err) {
          winston.error('error updating expiration on mysql', { error: err });
        }
      });
    }
  });
};

module.exports = MysqlDocumentStore;